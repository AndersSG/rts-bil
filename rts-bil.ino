#include <STM32FreeRTOS.h>

const int IN1_rightBack = D7;
const int IN2_rightForward = D3;
const int IN3_leftForward = D9;
const int IN4_leftBack = D8;

const int trigPin_left = D4;
const int echoPin_left = D2;
const int trigPin_right = D12;
const int echoPin_right = D13;


// Minimum distance for wall in cm 
const int minimumWallDistance = 10;
const int maximumWallDistance = 60;
const int distanceCheckInterval = 500;

// Declare a semaphore handle.
SemaphoreHandle_t sem;

typedef struct { 
  int wallDistance_left; 
  int wallDistance_right; 
} taskData;
taskData t1Data = {0};

static void Drive(void* arg);
static void ParseData(void* arg);

static int ReadDistance(void* arg, int trigPin, int echoPin);

void setup() {
  pinMode(IN1_rightBack, OUTPUT);
  pinMode(IN2_rightForward, OUTPUT);
  pinMode(IN3_leftForward, OUTPUT);
  pinMode(IN4_leftBack, OUTPUT);

  pinMode(trigPin_left, OUTPUT);
  pinMode(echoPin_left, INPUT);
  
  pinMode(trigPin_right, OUTPUT);
  pinMode(echoPin_right, INPUT);

  Serial.begin(9600);

  // initialize semaphore
  sem = xSemaphoreCreateCounting(1, 0);
  
  portBASE_TYPE driveTask, readTask;

  driveTask = xTaskCreate(Drive, NULL, configMINIMAL_STACK_SIZE, 
              &t1Data, 2, NULL);
  readTask = xTaskCreate(ParseData, NULL, configMINIMAL_STACK_SIZE, 
              &t1Data, 1, NULL);

  vTaskStartScheduler();

  while(1);
}

static void Drive(void* arg) {
  taskData *td = (taskData *)arg;
  
  while(1) {
    xSemaphoreTake(sem, portMAX_DELAY);

    int diff = (td->wallDistance_right) - (td->wallDistance_left);

    Serial.print("Diff "); 
    Serial.println(diff);
    
    analogWrite(IN4_leftBack, 0);
    analogWrite(IN1_rightBack, 0);

    int leftEnginePower;
    int rightEnginePower;
    
    if (diff < 0) {
      leftEnginePower = constrain(map(td->wallDistance_right, minimumWallDistance, maximumWallDistance, 0, 255), 0, 255);
      rightEnginePower = 255;
    }
    else {
      rightEnginePower = constrain(map(td->wallDistance_left, minimumWallDistance, maximumWallDistance, 0, 255), 0 , 255);
      leftEnginePower = 255;
    }
    
    analogWrite(IN3_leftForward, leftEnginePower);
    analogWrite(IN2_rightForward, rightEnginePower);
    
    Serial.print("Left engine Power: "); 
    Serial.println(leftEnginePower);
    Serial.print("Right engine Power: ");
    Serial.println(rightEnginePower);
  }
}

static void ParseData(void* arg) {
  taskData *td = (taskData *)arg;
  
  while(1) {
    int wallDistance_left = ReadDistance(arg, trigPin_left, echoPin_left);
    int wallDistance_right = ReadDistance(arg, trigPin_right, echoPin_right);
    
    td->wallDistance_left  = wallDistance_left;
    td->wallDistance_right  = wallDistance_right;
    Serial.print("Wall left: ");
    Serial.print(wallDistance_left);
    Serial.print("cm");
    Serial.println();
    Serial.print("Wall right: ");
    Serial.print(wallDistance_right);
    Serial.print("cm");
    Serial.println();
    
    xSemaphoreGive(sem);
    vTaskDelay(pdMS_TO_TICKS(distanceCheckInterval));
 }
}

static int ReadDistance(void* arg, int trigPin, int echoPin) {
  taskData *td = (taskData *)arg;
    
    // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
    // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
    digitalWrite(trigPin, LOW);
    vTaskDelay( pdMS_TO_TICKS(5) );
  
    digitalWrite(trigPin, HIGH);
    vTaskDelay( pdMS_TO_TICKS(1) );
  
    digitalWrite(trigPin, LOW);

    // Read the signal from the sensor: a HIGH pulse whose
    // duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    pinMode(echoPin, INPUT);
    long duration = pulseIn(echoPin, HIGH, 100000);

    // Convert the time into a distance and return it
    return (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
}

// Useless garbage function
void loop() {}
